Implement distributed training of a simple convolutional network on MNIST using
the high level torch distributed API. Test it first on one node and 2 GPUs (but
spawning 2 processes, one for each GPU), then try scaling it up to 2 nodes and
6 (or 8) GPUs. Work in groups, to prevent exceeding the cluster capacity.
For reference, you are encouraged to use:
https://github.com/pytorch/examples/tree/master/mnist
to get a grasp on how pytorch is working, and
https://github.com/facebookresearch/XLM/
to have a well coded example of how to handle SLURM in conjunction with pytorch.


import argparse
from os.path import exists, join

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--logdir', type=str, required=True)
    parser.add_argument('--p', type=float, required=True)
    args = parser.parse_args()

    assert exists(args.logdir), "Log directory non existant..."
    open(join(args.logdir, 'logs.txt'), 'w').write(str(args.p) + "\n")

We are nearly finished. Instead of a single script, we now want to launch a
serie of scripts, and store each of their log files in a different directory.
Write a launch_job_list function, that takes as argument a command, an
experiment name, a list of command line arguments, as a List of python Dict,
and launch the command on the cluster as many times as there are arguments.

Each time you call the function, a new log directory should be created, of
the form logs/{exp_name}/{year}_{month}_{day}_{hour}_{mins}_{secs}, as well as
subdirectories for each of the jobs, of the form
logs/{exp_name}/{year}_{month}_{day}_{hour}_{mins}_{secs}/{job_id}.
These subdirectories will serve as logdir for each of the command line arguments.

For example, a call to
launch_job_list(
	cmd='python material/script.py',
	exp_name='script',
	cmd_args=[{'p': 0}, {'p': 1}],
	ngpus=1,
	ncpus=1)
should create (if it does not already exist) a directory logs/script,
then a sub directory (which cannot already exist) logs/script/2019_02_16_14_02_26
(for instance), and two sub directories logs/script/2019_02_16_14_02_26/7829,
logs/script/2019_02_16_14_02_26/7830 (for instance), then launch, on the cluster
the commands 
python --logdir logs/script/2019_02_16_14_02_26/7829 -p 0
python --logdir logs/script/2019_02_16_14_02_26/7830 -p 1
and redirect the stderrs and stdouts of each of them to
logs/script/2019_02_16_14_02_26/7829.stdout, logs/script/2019_02_16_14_02_26/7829.stderr
logs/script/2019_02_16_14_02_26/7830.stdout, logs/script/2019_02_16_14_02_26/7830.stderr
respectively.

You can then try your script at scale by generating a very long list of
arguments (typically in the hundreds), and launch all of these scripts on the
cluster.

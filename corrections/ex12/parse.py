import argparse
import argload

def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--batch_size', type=int, default=1024)
    parser.add_argument('--test_batch_size', type=int, default=1024)
    parser.add_argument('--lr', type=float, default=0.01)
    parser.add_argument('--momentum', type=int, default=0.5)
    parser.add_argument('--epochs', type=int, default=100)
    parser = argload.ArgumentLoader(
        parser, to_reload=['batch_size', 'test_batch_size', 'lr', 'momentum', 'epochs'])
    return parser.parse_args()
